# Distributed Algorithm Tikz Generator 

## What is it?

This small Python script allows you to quickly create sketches describing
the communications of disctributed algorithms.

To do so, it generates a [Tikz](https://en.wikibooks.org/wiki/LaTeX/PGF/TikZ) output that you can then compile into a PDF.

__Disclaimer__: This project was initially made to help me create fast drawings
of my algorithms, at a time I needed to draw a lot of those. It is thus
made for one particular type of drawing and I don't intend to change that
for now.  
If you are looking for a general purpose tool, Tikz is what you need.  
If you want to tweak it to your need, feel free to do so.  


## Screenshots

![Example of a run of the script](./img/example_input.pdf)

![Corresponding created graph (after compilation with pdflatex)](./img/example_output.pdf)



## Licence

Copyright 2024

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Author

[Timothée Zerbib](https://gitlab.com/tzerbib)

