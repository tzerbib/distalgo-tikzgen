import os

PATH = "/tmp/algo_sketch.tex"
MIN  = 0.7 # x coordinate of the start of each node time line
UP   = 0.2 # y coordinate of the top of the phase delimiter separators

def main():
  # Output path
  var = input("Filename (default " + PATH + "): ")
  if var != "":
    current_dir = os.path.dirname(os.path.abspath(__file__))
    filename = current_dir + '/' + var
  else:
    filename = PATH

  # Set number of replicas (> 0)
  n = 0
  while True:
    try:
      n = int(input("Number of replicas = "))
    except:pass
    if n < 1:
      print("N should be a positive number (> 0)")
    else:
      break


  file = open(filename, "w")
  header(file)
  phases = design()
  draw(file, n, phases)
  footer(file)
  file.close()

  print("File written at " + filename)
  

def design():
  i = 1
  phases = []

  print("")
  while (phase := input("\n**** Phase " + str(i) + " name (leave empty to end): ")) != "":
    phase_comp = input("  Phase subtitle: ")
    origin = input("  from (n for all, c for client, i in [0;N-1] for replica i): ")
    dest = input("  to (n for all, c for client, i in [0;N-1] for replica i): ")
    phases.append({"pname":phase, "pcomp":phase_comp, "from":origin, "to":dest})
    i = i+1

  return phases


def draw(f, n, phases):
  f.write("    \\def\min{" + str(MIN) + "}\n")
  f.write("    \\def\max{" + str(len(phases) +1.5) + "}\n")
  f.write("\n")

  draw_clients(f, n, phases)
  draw_replicas(f, n, phases)
  draw_comms(f, n, phases)
  draw_phase_delimiter(f, n, phases)

  f.write("\n")


def draw_clients(f, n, phases):
  f.write("\n    <!-- c -->\n")
  f.write("    \\def\\c{0}\n")
  f.write("    \\node [] at (0,\\c){$\mathit{Client}$};\n")
  f.write("    \\draw [->] (\\min,\\c) -- (\\max,\\c);\n")

  # Phases
  for i in range(len(phases)):
    written = False
    # From client
    if isClient(phases[i]["from"]) :
      written = True
      f.write("    \\coordinate (c_" + phases[i]["pname"] +"_0) at (" + str(i+1) + ".1 ,\\c);\n")
    # To client
    if isClient(phases[i]["to"]) :
      written = True
      # From everyone
      if isAll(phases[i]["from"]):
        step = (.95-.55)/(n+1)
        for j in range(n):
          f.write("    \\coordinate (c_" + phases[i]["pname"] +"_" + str(j+1) + ") at (" + str(round(i+1.55+step*(j+1), 2)) + ",\\c);\n")
      # Single sender
      else:
        f.write("    \\coordinate (c_" + phases[i]["pname"] +"_1) at (" + str(i+1) + ".9 ,\\c);\n")
    if written:
      f.write("\n")


def draw_replicas(f, n, phases):
  # For each replica
  for r in range(n):
    node = "r" + str(r)
    f.write("    <!-- " + node + " -->\n")
    f.write("    \\def\\"+ node +"{-" + str(r+1) +"}\n")
    f.write("    \\node [] at (0.3,\\" + node + "){$R_" + str(r) + "$};\n")
    f.write("    \\draw [->] (\\min,\\" + node + ") -- (\\max,\\" + node + ");\n")

    # Phases
    for i in range(len(phases)):
      written = False
      # From replica
      if phases[i]["from"] == str(r) or isAll(phases[i]["from"]):
        written = True
        f.write("    \\coordinate (" + node + "_" + phases[i]["pname"] + "_0) at (" + str(i+1) + ".1,\\" + node + ");\n")
      # To Replica
      if phases[i]["to"] == str(r) or isAll(phases[i]["to"]):
        written = True
        # From all
        if isAll(phases[i]["from"]):
          step = (.95-.55)/(n+1)
          for j in range(n):
            f.write("    \\coordinate (" + node + "_" + phases[i]["pname"] + "_" + str(j+1) + ") at (" + str(round(i+1.55+step*(j+1), 2)) + ",\\" + node + ");\n")
        # Single sender
        else:
          f.write("    \\coordinate (" + node + "_" + phases[i]["pname"] + "_1) at (" + str(i+1) + ".9,\\" + node + ");\n")
      if written:
        f.write("\n")
  
  f.write("\n")
    

def draw_comm_1o1(f, sender, receiver, phase, sposition, rposition):
  s = "c_"  if isClient(sender) else "r" + sender + "_" # write c_ for client or r_ for replica
  s = s + phase + "_" + str(sposition) # Add the phase and the position

  r = "c_"  if isClient(receiver) else "r" + receiver + "_" # write c_ for client or r_ for replica
  r = r + phase + '_' + str(rposition) # Add the phase and the position (possible multiple received messages for the phase)

  if (sender) == receiver:
    f.write("    \\draw[->, thick, line width=0.2mm, color=blue] (" + s + ") to [bend left=90] (" + r +");\n")
  else:
    f.write("    \\draw[->, thick, line width=0.2mm, color=blue] (" + s + ") -- (" + r +");\n")


def draw_comm_no1(f, n, target, pname):
  t = -1 if target == 'c' else int(target)

  # The node itself if not the client
  if target != 'c':
    draw_comm_1o1(f, target, target, pname, 0, 1)

  i = 1 if target != 'c' else 0
  dist = 0
  while i < n:
    dist = dist+1

    # The node above at distance dist
    if t + dist < n:
      i = i+1
      draw_comm_1o1(f, str(t+dist), target, pname, 0, i)
    
    # The node below at distance dist
    if t - dist >= 0:
      i = i+1
      draw_comm_1o1(f, str(t-dist), target, pname, 0, i)


def draw_comms(f, n, phases):
  for i in range(len(phases)):
    # Phase title (<!-- comment --> )
      f.write("\n    <!-- " + phases[i]["pname"] + " -->\n")

      # From All
      if isAll(phases[i]["from"]):
        # To All
        if isAll(phases[i]["to"]):
          for j in range(n):
            draw_comm_no1(f, n, str(j), phases[i]["pname"])
        # To one
        else:
          draw_comm_no1(f, n, phases[i]["to"], phases[i]["pname"])
      # Single sender
      else:
        # To All
        if isAll(phases[i]["to"]):
          sender = "c_"  if isClient(phases[i]["from"]) else "r" + str(phases[i]["from"]) + "_" # write c_ for client or r_ for replica
          sender = sender + phases[i]["pname"] + "_0" # Add the rest of the coordinate name
          for r in range(n):
            draw_comm_1o1(f, phases[i]["from"], str(r), phases[i]["pname"], 0, 1)
        else:
          draw_comm_1o1(f, phases[i]["from"], phases[i]["to"], phases[i]["pname"], 0, 1)


def draw_phase_delimiter(f, n, phases):
  f.write("\n\n\n    <!-- Phase delimiters -->\n")
  f.write("    \\def\\up{" + str(UP) + "}\n")
  f.write("    \\def\\down{" + str(-(n+.2)) + "}\n")

  for i in range(len(phases)):
    pname = phases[i]["pname"]
    f.write("\n    <!-- " + pname + " -->\n")
    f.write("    \\coordinate (" + pname + "_up) at (" + str(i+2) + ", \\up);\n")
    f.write("    \\coordinate (" + pname + "_down) at (" + str(i+2) + ", \\down);\n")
    f.write("    \\draw[dotted] (" + pname + "_up) -- (" + pname + "_down);\n")

    if phases[i]["pcomp"] != "":
      f.write("    \\draw[align=center, execute at begin node=\\setlength{\\baselineskip}{0.55ex}] (" + str(i+1.5) + ", 0.3) node {\\footnotesize " + pname + "\\\\\\scriptsize($" + phases[i]["pcomp"] + "$)};\n")
    else:
      f.write("    \\draw (" + str(i+1.5) + ", 0.25) node {\\footnotesize " + pname + "};\n")


def header(f):
  
  f.write("\\documentclass{standalone}\n")
  f.write("\\usepackage{tikz}\n\n")
  f.write("\\begin{document}\n")
  f.write("  \\begin{tikzpicture}\n")
  f.write("    \\usetikzlibrary{arrows.meta}\n\n")


def footer(f):
  f.write("  \\end{tikzpicture}\n")
  f.write("\\end{document}\n")
  f.write("\n")

def isClient(s):
  return(s == 'c' or s == 'C' or s == "Client" or s == "client")

def isAll(s):
  return(s == 'n' or s == 'N' or s == "a" or s == "A" or s == "all" or s == "All")


if __name__ == "__main__":
  main()
